<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::get('/reportes/recorrido-punto-a-punto', 'RecorridoController@index');

Route::get('/reportes/kilometros-recorridos', 'KmsRecorridosController@index');

Route::get('/reportes/actividad', 'ActividadController@index');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
