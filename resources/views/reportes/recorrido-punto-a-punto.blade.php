@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Recorrido punto a punto</div>

                    <div class="card-body">
                        <table id="recorridopuntoapunto" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Patente</th>
                                <th>Fecha</th>
                                <th>Velocidad</th>
                                <th>Info</th>
                                <th>Kms</th>
                                <th>Coordenadas</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Patente</th>
                                <th>Fecha</th>
                                <th>Velocidad</th>
                                <th>Info</th>
                                <th>Kms</th>
                                <th>Coordenadas</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            @foreach($recorrido as $punto)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ $punto["patente"] }}</td>
                                    <td>{{ Carbon\Carbon::parse($punto["fecha"])->format('d-m-Y H:i:s') }}</td>
                                    <td>{{ $punto["velocidad"] }}</td>
                                    <td>{{ $punto["estado"] }}</td>
                                    <td>{{ $punto["distancia"] }}</td>
                                    <td>{{ $punto["ubicacion"] }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#recorridopuntoapunto').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                "lengthMenu": [5, 10, 20, 40, 60, 80, 100],
                "pageLength": 5
            });
        } );
    </script>
@endpush