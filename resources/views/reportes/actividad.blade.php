@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Actividad</div>

                    <div class="card-body">
                        <table id="actividad" class="table table-striped table-responsive table-bordered" cellspacing="0" width="100%">
                            <thead>
                            <tr>
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Inicio Jornada</th>
                                <th>Fin Jornada</th>
                                <th>Total Hrs Jornada</th>
                                <th>Detenciones</th>
                                <th>Vel Max</th>
                                <th>Excesos Vel</th>
                                <th>Kms</th>
                            </tr>
                            </thead>
                            <tfoot>
                            <tr>
                                <th>#</th>
                                <th>Fecha</th>
                                <th>Inicio Jornada</th>
                                <th>Fin Jornada</th>
                                <th>Total Hrs Jornada</th>
                                <th>Detenciones</th>
                                <th>Vel Max</th>
                                <th>Excesos Vel</th>
                                <th>Kms</th>
                            </tr>
                            </tfoot>
                            <tbody>
                            <tr>
                                <td></td>
                                <td>Total</td>
                                <td></td>
                                <td></td>
                                <td></td>
                                <td>{{ $totales["total_detenciones"] }}</td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                            @foreach($datos as $dato)
                                <tr>
                                    <td>{{ $loop->iteration }}</td>
                                    <td>{{ \Carbon\Carbon::parse($dato["fecha"])->format('d-m-Y') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($dato["primero"]["fecha"])->format('H:i:s') }}</td>
                                    <td>{{ \Carbon\Carbon::parse($dato["ultimo"]["fecha"])->format('H:i:s') }}</td>
                                    <td>{{ $dato["tiempo_total"] }}</td>
                                    <td>{{ $dato["detenciones"] }}</td>
                                    <td>{{ $dato["velocidad_maxima"] }}</td>
                                    <td>{{ $dato["excesos_velocidad"] }}</td>
                                    <td>{{ number_format($dato["diferencia"], 1, ',', '.')  }}</td>
                                </tr>
                            @endforeach
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection

@push('styles')
    <link rel="stylesheet" href="https://cdn.datatables.net/1.10.16/css/dataTables.bootstrap4.min.css">
@endpush

@push('scripts')
    <script src="https://cdn.datatables.net/1.10.16/js/jquery.dataTables.min.js"></script>
    <script src="https://cdn.datatables.net/1.10.16/js/dataTables.bootstrap4.min.js"></script>
    <script>
        $(document).ready(function() {
            $('#actividad').DataTable({
                "language": {
                    "url": "//cdn.datatables.net/plug-ins/1.10.16/i18n/Spanish.json"
                },
                "lengthMenu": [5, 10, 20, 40, 60, 80, 100],
                "pageLength": 5
            });
        } );
    </script>
@endpush