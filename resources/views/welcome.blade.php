@extends('layouts.app')

@section('content')
    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">Inicio</div>
                    <p class="card-body">Selecciona uno de los reportes en el menu superior izquierdo</p>
                </div>
            </div>
        </div>
    </div>
@endsection
