<?php

namespace App\Http\Controllers;

use App\Repositories\RegistrosRepository;
use Carbon\Carbon;

class KmsRecorridosController extends Controller
{
    protected $registros;

    public function __construct(RegistrosRepository $registrosRepository)
    {
        $this->registros = $registrosRepository->obtenerTodos();
    }

    public function index()
    {
        $recorrido = collect([]);


        $sumatoria = [];

        $dias = 0;

        $actual = null;
        $presente = null;
        $inicial = null;

        foreach($this->registros as $indice => $punto)
        {
            $actual = Carbon::parse($punto["fecha"])->day;

            if ($presente == null) {
                $presente = $actual;
                $sumatoria[]["inicial"] = $punto["distancia"];
                $sumatoria[0]["fecha"] = $punto["fecha"];
            }

            if ($presente == $actual) {
                if ($punto === end($this->registros)) {
                    $sumatoria[$dias]["final"] = $punto["distancia"];
                    $sumatoria[$dias]["dia"] = $presente;
                    $sumatoria[$dias]["fecha"] = $punto["fecha"];
                    $sumatoria[$dias]["diferencia"] = ($sumatoria[$dias]["final"] - $sumatoria[$dias]["inicial"]) / 1000;
                }
            } else {
                $sumatoria[$dias]["dia"] = $presente;
                $presente = $actual;
                $sumatoria[]["inicial"] = $punto["distancia"];
                $sumatoria[$dias]["final"] = $punto["distancia"];
                $sumatoria[$dias]["diferencia"] = ($sumatoria[$dias]["final"] - $sumatoria[$dias]["inicial"]) / 1000;
                $dias++;
            }
        }

        $total = 0;

        foreach($sumatoria as $indice => $suma) {
            $total = $total + $suma["diferencia"];
            $sumatoria[$indice]["total"] = $total;
        }

        return view('reportes.kilometros-recorridos')->with([
            'datos' => $sumatoria
        ]);
    }
}
