<?php

namespace App\Http\Controllers;

use App\Repositories\RegistrosRepository;

class RecorridoController extends Controller
{
    /**
     * Almacena el registro previo al actual durante la iteración de todos los registros
     * @var
     */
    protected $previo;

    /**
     * Almacena el registro siguiente al actual durante la iteración de todos los registros
     * @var
     */
    protected $siguiente;

    /**
     * Almacena el registro actual durante la iteración de todos los registros
     * @var
     */
    protected $actual;

    /**
     * Almacena el indice del registro actual durante la iteración de todos los registros
     * @var
     */
    protected $indice;

    /**
     * Almacena el primer registro que puede ser impreso durante la iteración de todos los registros
     * @var
     */
    protected $primero;

    /**
     * Almacena un booleano que define si fue detectado un motor apagado
     * @var
     */
    protected $motorApagadoFueDetectado;

    /**
     * Almacena todos los registros
     * @var array
     */
    protected $registros;

    public function __construct(RegistrosRepository $registrosRepository)
    {
        $this->registros = $registrosRepository->obtenerTodos();
    }

    public function index()
    {
        // El recorrido vacio
        $recorrido = collect([]);

        // Todavía no existe un primero
        $this->primero = null;

        // Las alertas están encendidas!
        $this->motorApagadoFueDetectado = false;

        // Recorrer todos los registros
        foreach( $this->registros as $indice => $punto )
        {
            $this->indice = $indice + 1;
            $this->actual = $punto;

            $this->asignarAnteriorSiguiente($indice);

            /**
             * Obtener el tipo de recorrido del punto
             */
            $tipo = $this->obtenerTipoRecorrido($punto);

            /**
             * En caso de que sea el primero debe cumplir ciertas condiciones:
             *
             */
            if ( $this->primero == null) {

                /**
                 * Que el tipo sea "Detenido motor apagado" o que el tipo sea "En marcha"
                 */
                if ( $tipo == "Detenido motor apagado" || $tipo == "En marcha" ) {

                    /**
                     * En caso de que el tipo sea "En marcha" añadir al recorrido
                     */
                    if( $tipo == "En marcha" ) {
                        $recorrido->push($this->insertarPuntoActual($tipo));

                    } else {

                        /**
                         * En caso de que el siguiente no sea un "Detenido motor apagado" añadir al recorrido
                         */
                        if ( ! $this->siguienteEs("Detenido motor apagado" ) ) {
                            $recorrido->push($this->insertarPuntoActual($tipo));

                        }
                    }
                }

            } else {

                /**
                 * En el caso de que no sea el primero
                 */

                /**
                 * Si el tipo es "Motor apagado" entonces detectar que el motor fue apagado
                 */
                if ($tipo == "Motor apagado")
                    $this->motorApagadoFueDetectado = true;

                /**
                 * En caso de que el motor fue apagado
                 */
                if ( $this->motorApagadoFueDetectado ) {

                    /**
                     * Si el tipo es "Detenido motor apagado" y el siguiente también lo es
                     */
                    if ( $tipo == "Detenido motor apagado" && $this->siguienteEs("Detenido motor apagado" ) ) {

                        // Omitir el añadir al recorrido

                    } else {

                        /**
                         * Si el tipo es "Motor apagado" añadirlo al recorrido
                         */
                        if ( $tipo == "Motor apagado" ) {
                            $recorrido->push($this->insertarPuntoActual($tipo));

                        } else {

                            /**
                             * Si no es un "Motor apagado" añadirlo al recorrido
                             */
                            $recorrido->push($this->insertarPuntoActual($tipo));
                            $this->motorApagadoFueDetectado = false;
                        }
                    }
                } else {
                    /**
                     * Si no fue detectado un motor apagado entonces añadir al recorrido
                     */
                    $recorrido->push($this->insertarPuntoActual($tipo));
                }
            }
        }


        return view('reportes.recorrido-punto-a-punto')->with([
            'recorrido' => $recorrido,
        ]);
    }

    /**
     * Añade el punto actual al recorrido
     *
     * @param $estado
     * @return array
     */
    public function insertarPuntoActual($estado) {
        if ($this->primero == null) {
            $this->primero = $this->actual;
        }

        return [
            "id" => $this->indice,
            "patente" => $this->actual["placa"],
            "fecha" => $this->actual["fecha"],
            "estado" => $estado,
            "velocidad" => $this->actual["velocidad"]. " kms/h",
            "distancia" => $this->formatearDistancia($this->actual["distancia"]),
            "ubicacion" => $this->actual["latitud"] . "," . $this->actual["longitud"],
        ];
    }

    /**
     * Formatea la distancia de un punto a kilometros
     *
     * @param $distancia
     * @return string
     */
    public function formatearDistancia($distancia) {
        $kilometros = (intval($distancia) / 1000);

        $kilometros_formateados = number_format($kilometros, 2, ',', '.');

        return $kilometros_formateados . " kms";
    }

    /**
     * Obtiene el tipo del recorrido
     *
     * @param $punto
     * @return string
     */
    public function obtenerTipoRecorrido($punto) {
        $tipo = "";

        if ( $punto["modo"] === "33" ) {
            $tipo = "Motor encendido";

            // Si tiene velocidad y su estado es encendido entonces está "En marcha"
        } elseif ( str_contains($punto["estado_io"], '110000') && $punto["velocidad"] !== "0") {
            $tipo = "En marcha";

            // Si su estado es apagado, entonces está "Detenido motor apagado"
        } elseif ( str_contains($punto["estado_io"], '110000') && $punto["velocidad"] === "0" ) {
            $tipo = "Detenido motor encendido";

            // Si su modo es 34 entonces es un evento de notificación que tiene el "Motor apagado"
        } elseif ( $punto["modo"] === "34" ) {
            $tipo = "Motor apagado";

            // Si su modo es 33 entonces es un evento de notificación que tiene el "Motor encendido"
        } elseif ( str_contains($punto["estado_io"], '010000') ) {
            $tipo = "Detenido motor apagado";
        }

        return $tipo;
    }

    /**
     * Asigna los puntos colindantes del punto actual
     * @param $index
     */
    public function asignarAnteriorSiguiente($indice)
    {
        if( $indice > 0 ) {
            $this->previo = $this->registros[($indice - 1)];
        }

        if( $indice < (count($this->registros) - 1) ) {
            $this->siguiente = $this->registros[($indice + 1)];
        }
    }

    public function siguienteEs($tipo)
    {
        $tipo_siguiente = $this->obtenerTipoRecorrido($this->siguiente);

        if ($tipo == $tipo_siguiente) {
            return true;
        } else {
            return false;
        }
    }

    public function anteriorEs($tipo)
    {
        $tipo_previo = $this->obtenerTipoRecorrido($this->previo);

        if ($tipo == $tipo_previo) {
            return true;
        } else {
            return false;
        }
    }
}
