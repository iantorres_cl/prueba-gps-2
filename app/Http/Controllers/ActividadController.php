<?php

namespace App\Http\Controllers;

use App\Repositories\RegistrosRepository;
use Carbon\Carbon;

class ActividadController extends Controller
{
    protected $registros;

    public function __construct(RegistrosRepository $registrosRepository)
    {
        $this->registros = $registrosRepository->obtenerTodos();
    }

    public function index()
    {
        $recorrido = collect([]);


        $sumatoria = [];

        $dias = 0;

        $actual = null;
        $presente = null;
        $inicial = null;

        foreach($this->registros as $indice => $punto)
        {
            $actual = Carbon::parse($punto["fecha"])->day;

            if ($presente == null) {
                $presente = $actual;
                $sumatoria[]["inicial"] = $punto["distancia"];
                $sumatoria[0]["fecha"] = $punto["fecha"];
            }

            if ($presente == $actual) {
                if ($punto === end($this->registros)) {
                    $sumatoria[$dias]["final"] = $punto["distancia"];
                    $sumatoria[$dias]["dia"] = $presente;
                    $sumatoria[$dias]["fecha"] = $punto["fecha"];
                    $sumatoria[$dias]["diferencia"] = ($sumatoria[$dias]["final"] - $sumatoria[$dias]["inicial"]) / 1000;
                }
            } else {
                $sumatoria[$dias]["dia"] = $presente;
                $presente = $actual;
                $sumatoria[]["inicial"] = $punto["distancia"];
                $sumatoria[$dias]["final"] = $punto["distancia"];
                $sumatoria[$dias]["diferencia"] = ($sumatoria[$dias]["final"] - $sumatoria[$dias]["inicial"]) / 1000;
                $dias++;
            }

            if($this->obtenerTipoRecorrido($punto) === "Motor encendido" || $this->obtenerTipoRecorrido($punto) === "Motor apagado") {
                if($this->obtenerTipoRecorrido($punto) === "Motor encendido") {
                    if(!isset($sumatoria[$dias]["primero"])) {
                        $sumatoria[$dias]["primero"] = [
                          "fecha" => $punto["fecha"]
                        ];
                    }
                } else {
                    $time = Carbon::parse($sumatoria[$dias]["primero"]["fecha"])->format('H:i:s');
                    $time2 = Carbon::parse($punto["fecha"])->format('H:i:s');

                    $secs = strtotime($time)-strtotime("00:00:00");
                    $result = date("H:i:s",strtotime($time2)-$secs);

                    $sumatoria[$dias]["ultimo"] = [
                        "fecha" => $punto["fecha"]
                    ];

                    $sumatoria[$dias]["tiempo_total"] = $result;
                }
            }

            if($this->obtenerTipoRecorrido($punto) === "Detenido motor encendido"){
                if(!isset($sumatoria[$dias]["detenciones"])){
                    $sumatoria[$dias]["detenciones"] = 0;
                } else {
                    $sumatoria[$dias]["detenciones"] = $sumatoria[$dias]["detenciones"] + 1;
                }
            }

            if(!isset($sumatoria[$dias]["velocidad_maxima"])) {
                $sumatoria[$dias]["velocidad_maxima"] = intval($punto["velocidad"]);
            } else {
                if($sumatoria[$dias]["velocidad_maxima"] < intval($punto["velocidad"])) {
                    $sumatoria[$dias]["velocidad_maxima"] = intval($punto["velocidad"]);
                }
            }

            if(!isset($sumatoria[$dias]["excesos_velocidad"])) {
                $sumatoria[$dias]["excesos_velocidad"] = 0;
            }

            if(intval($punto["velocidad"]) > 120) {
                if(!isset($sumatoria[$dias]["excesos_velocidad"])) {
                    $sumatoria[$dias]["excesos_velocidad"] = 1;
                } else {
                    $sumatoria[$dias]["excesos_velocidad"] = $sumatoria[$dias]["excesos_velocidad"] + 1;
                }
            }
        }

        $total = 0;

        $total_detenciones = 0;

        $totales = [];

        foreach($sumatoria as $indice => $suma) {
            $total = $total + $suma["diferencia"];
            $total_detenciones =  $total_detenciones + $suma["detenciones"];
            $sumatoria[$indice]["total"] = $total;
            $totales["total_detenciones"] = $total_detenciones;
        }

        return view('reportes.actividad')->with([
            'datos' => $sumatoria,
            'totales' => $totales,
        ]);
    }

    public function obtenerTipoRecorrido($punto) {
        $tipo = "";

        if ( $punto["modo"] === "33" ) {
            $tipo = "Motor encendido";

            // Si tiene velocidad y su estado es encendido entonces está "En marcha"
        } elseif ( str_contains($punto["estado_io"], '110000') && $punto["velocidad"] !== "0") {
            $tipo = "En marcha";

            // Si su estado es apagado, entonces está "Detenido motor apagado"
        } elseif ( str_contains($punto["estado_io"], '110000') && $punto["velocidad"] === "0" ) {
            $tipo = "Detenido motor encendido";

            // Si su modo es 34 entonces es un evento de notificación que tiene el "Motor apagado"
        } elseif ( $punto["modo"] === "34" ) {
            $tipo = "Motor apagado";

            // Si su modo es 33 entonces es un evento de notificación que tiene el "Motor encendido"
        } elseif ( str_contains($punto["estado_io"], '010000') ) {
            $tipo = "Detenido motor apagado";
        }

        return $tipo;
    }
}
